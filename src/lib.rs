#![doc = include_str!("../README.md")]
use proc_macro::TokenStream;
use syn::parse_macro_input;
use quote::quote;

mod sorted;
mod keywords;

use keywords:: { TransitionTableInfo, TransitionTableBody };
use sorted::SortedTable;

/// Sorting `const` array by `&str` as key.
///
/// As the result, the array can be searchable using [binary_search].
///
/// This macro can sort an array has '[(&str, T)]' type signature.
///
/// [binary_search]: https://doc.rust-lang.org/std/primitive.slice.html#method.binary_search
#[proc_macro_attribute]
pub fn sorted(_args: TokenStream, input: TokenStream) -> TokenStream {
    let result = parse_macro_input!(input as SortedTable);

    quote! { #result }.into()
}

/// Adding the transition table (trie tree) to the `const` array.
///
/// The added trie tree can be used with [keyword] parser.
/// The name of added table should be specified by this macro's argument.
/// The source array should be '[(&str, T)]' type.
///
/// [keyword]: https://docs.rs/keyword-parser/*/keyword_parser/transition/fn.keyword.html
#[proc_macro_attribute]
pub fn keywords(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut result = input.clone();
    let info = parse_macro_input!(args as TransitionTableInfo);
    let body = parse_macro_input!(input as TransitionTableBody);

    result.extend(Into::<TokenStream>::into(quote! { #info #body }));
    result
}