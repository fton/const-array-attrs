#[test]
fn tests() {
    let t = trybuild::TestCases::new();
    t.pass("examples/sorted.rs");
    t.pass("examples/keywords.rs");
    //t.compile_fail("tests/03-out-of-order.rs");
    //t.compile_fail("tests/04-variants-with-data.rs");
    //t.compile_fail("tests/05-match-expr.rs");
    //t.compile_fail("tests/06-pattern-path.rs");
    //t.compile_fail("tests/07-unrecognized-pattern.rs");
}
