[![Latest Release](https://gitlab.com/fton/const-array-attrs/-/badges/release.svg)](https://gitlab.com/fton/const-array-attrs/-/releases)
[![pipeline status](https://gitlab.com/fton/const-array-attrs/badges/main/pipeline.svg)](https://gitlab.com/fton/const-array-attrs/-/commits/main)
[![coverage report](https://gitlab.com/fton/const-array-attrs/badges/main/coverage.svg)](https://gitlab.com/fton/const-array-attrs/-/commits/main)
# Description
Attribute macros for const array.
# Usage
```rust
use const_array_attrs::sorted;

#[sorted]
pub const KEYWORDS: [(&'static str, u8); 3] = [
    ("Pa", 1),
    ("N", 2),
    ("ft", 3),
];

let mut it = KEYWORDS.iter();

assert_eq!(it.next(), Some(&("N", 2)));
assert_eq!(it.next(), Some(&("Pa", 1)));
assert_eq!(it.next(), Some(&("ft", 3)));
```
# Project status
This package is in the very early stage.