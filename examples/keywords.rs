use const_array_attrs::keywords;

#[keywords(pub(crate) TRANSITION_TBL)]
const KEYWORDS: [(&'static str, i8); 3] = [
    ("A", 1),
    ("BB", 2),
    ("BBC", 3),
];

fn main() {
    let mut it = TRANSITION_TBL.iter();

    assert_eq!(it.next(), Some(&(b'C', 0, 0, Some(&3))));
    assert_eq!(it.next(), Some(&(b'B', 0, 1, Some(&2))));
    assert_eq!(it.next(), Some(&(b'A', 0, 0, Some(&1))));
    assert_eq!(it.next(), Some(&(b'B', 1, 2, None)));
    assert_eq!(it.next(), Some(&(0, 2, 4, None)));
    assert!(it.next().is_none());
}