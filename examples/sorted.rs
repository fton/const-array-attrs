use const_array_attrs::sorted;

#[sorted]
pub const KEYWORDS: [(&'static str, u8); 3] = [
    ("Pa", 1),
    ("N", 2),
    ("ft", 3),
];

fn main() {
    let mut it = KEYWORDS.iter();

    assert_eq!(it.next(), Some(&("N", 2)));
    assert_eq!(it.next(), Some(&("Pa", 1)));
    assert_eq!(it.next(), Some(&("ft", 3)));
}