use const_array_attrs::sorted;

#[sorted]
pub const KEYWORDS: [(char, ()); 10] = [
    ('*', ()),
    ('\u{00D7}', ()), // ×
    ('\u{2715}', ()), // ✕
    ('\u{2716}', ()), // ✖
    ('\u{00B7}', ()), // ·
    ('\u{2022}', ()), // •
    ('\u{2219}', ()), // ∙
    ('\u{22C5}', ()), // ⋅
    ('\u{30FB}', ()), // ・
    ('\u{FF65}', ()) // ･
];

fn main() {
    for (i, &(c, _)) in KEYWORDS.iter().enumerate() {
        eprintln!("{}: {}", i, c);
    }
}
